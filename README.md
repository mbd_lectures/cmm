# Continuum Mechanical Modeling for Simulation Science {.unnumbered}

This website is published under the CC BY-NC-SA 4.0 license ([go to license](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)). If you want to refer to the content of this webpage, please cite 

```
@online{CMM,
  author = {Julia Kowalski, Ingo Steldermann, Alan Correa, Marc S. Boxberg},
  title = {Continuum Mechanical Modeling for Simulation Science},
  year = 2024,
  url = {https://mbd_lectures.pages.rwth-aachen.de/cmm},
  urldate = {YYYY-MM-DD}
}
```

## About the course

The course is lectured by [Prof. Julia Kowalski](https://www.mbd.rwth-aachen.de/cms/mbd/der-lehrstuhl/team/~qashd/julia-kowalski/?allou=1), head of the [Chair of Methods for Model-based Development in Computational Engineering](https://www.mbd.rwth-aachen.de/cms/~qamyz/mbd/lidx/1/) as part of the Simulation Science study program at RWTH Aachen University. It is designed as a 14 week graduate course with 2 lectures and 1 tutorial each week.

## Required preliminary knowledge

Calculus, linear algebra and basic knowledge of numerical methods for
partial differential equations. Basic knowledge on fluidmechanics and
thermodynamics is an advantage, but no must.

## How to contribute?

This webpage provides material that complements in person lectures and literature that has been provided. Should you spot any typos or errors, we'd be grateful if you supported us via creating an issue [here](https://git.rwth-aachen.de/mbd_lectures/cmm/-/issues). You can find a list of all helping hands [here](https://mbd_lectures.pages.rwth-aachen.de/cmm/content/wiki/contributing.html).
